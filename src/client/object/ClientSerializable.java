package client.object;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.model.Message;

public class ClientSerializable {
	public void connect() throws UnknownHostException, IOException, ClassNotFoundException {
		
		Socket socket = new Socket("localhost", 1989);
		
		//sending to server
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		Message message = new Message("Hello", "Study and share");
		
		oos.writeObject(message);
		
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		
		Message ms2 = (Message)ois.readObject();
		if (ms2 != null) {
			System.out.println("Receive from server: " + ms2.getTitle() + "====" + ms2.getBody());
		}
		
		
		
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		ClientSerializable client = new ClientSerializable();
		client.connect();
	}
}
